package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class InformationServlet extends HttpServlet {

	private static final long serialVersionUID = 8880012107939109054L;
	private ArrayList<String> data;

	public void init() throws ServletException {
		data = new ArrayList<>(Arrays.asList("Standard", "Deluxe"));
		System.out.println("*****************************************");
		System.out.println("Information Servlet has been initialized.");
		System.out.println("*****************************************");
	}
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		PrintWriter out = response.getWriter();
		ServletContext servletContext = getServletContext();
		ServletConfig servletConfig = getServletConfig();
		String hotelName = servletContext.getInitParameter("hotel_name");
		String hotelAddress = servletContext.getInitParameter("hotel_address");
		String hotelContact = servletContext.getInitParameter("hotel_contact");
		String hotelStatus = servletConfig.getInitParameter("status");
		
		// Get data from the RoomServlet created under the doGet()
		// String hotelFacilities = System.getProperty("facilities");
		
		// Pass data via send redirect
		String facilities = request.getParameter("facilities");
		
		// Pass data HttpSession
		HttpSession session = request.getSession();
		String availableRooms = session.getAttribute("availableRooms").toString();
				
		out.println(
				"<h1>Hotel Information</h1>" + 
				"<p>Hotel Name: " + hotelName + "</p>" +
				"<p>Hotel Address: " + hotelAddress + "</p>" +
				"<p>Hotel Contact: " + hotelContact + "</p>" +
				"<p>Hotel Status: " + hotelStatus + "</p>" +
				"<p>Hotel Facilities: " + facilities + "</p>" +
				"<p>Available Rooms: " + availableRooms + "</p>" 
			);
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String roomName = request.getParameter("room-type");
		PrintWriter out = response.getWriter();
		
		data.add(roomName);
		out.println(data);
	}
	
	public void doPut(HttpServletRequest request, HttpServletResponse response) throws IOException {
		PrintWriter out = response.getWriter();
		
		data.set(1, "Promo");	
		out.println(data);
	}
	
	public void doDelete(HttpServletRequest request, HttpServletResponse response) throws IOException {
		PrintWriter out = response.getWriter();
		
		data.remove(1);	
		out.println(data);
	}

	public void destroy() {
		System.out.println("***************************************");
		System.out.println("Information Servlet has been destroyed.");
		System.out.println("***************************************");
	}
	
}
