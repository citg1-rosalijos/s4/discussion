package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class RoomServlet extends HttpServlet {

	private static final long serialVersionUID = 7802515668491130811L;
	private ArrayList<String> data = new ArrayList<>();

	public void init() throws ServletException {
		System.out.println("*****************************************");
		System.out.println("Room Servlet has been initialized.");
		System.out.println("*****************************************");
	}
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		PrintWriter out = response.getWriter();
		
		// Sharing data via system properties
		System.getProperties().put("facilities", "Swimming Pool, Gym, Ballroom, Offices");
		String facilities = System.getProperty("facilities");
		out.println(facilities);
		
		// Session Management
		// Sending information by writing the url of the information class
		// This also redirectst he servlet to the information servlet and passing the data via query string
		
		// Sending Information via HttpSession
		HttpSession session = request.getSession();
		session.setAttribute("availableRooms", "Standard");
		
		response.sendRedirect("information?facilities=" + facilities);
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		String roomName = request.getParameter("room-type");
		PrintWriter out = response.getWriter();
		ServletContext servletContext = getServletContext();
		
		// Add attribute to the Servlet Context
		servletContext.setAttribute("data", data);
		
		// The Request Dispatcher 
		//  will forward the request and response bodies to the other servlet
		// will invoke the doPost() method of that servlet (in this case, InformationServlet
		RequestDispatcher rd = request.getRequestDispatcher("information");
		rd.forward(request, response);
		
		data.add(roomName);
		out.println(data);
	}
	
	public void destroy() {
		System.out.println("***************************************");
		System.out.println("Room Servlet has been destroyed.");
		System.out.println("***************************************");
	}
}
